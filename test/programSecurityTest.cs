using System;
using NUnit.Framework;

namespace program.Tests
{
    [TestFixture, Category("security")]
    public class programSecurityTest
    {
        [Test]
        public void withdraw_negative_amount_throw_exception()
        {
            Balance balance = new Balance(1000);
            Amount amount = new Amount(-1000);
            Assert.Throws<ArgumentOutOfRangeException>(() => 
                    Program.performTransfer(amount, balance));
        }
        [Test]
        public void withdraw_Int32Max_throw_exception()
        {
            Balance balance = new Balance(1000);
            Amount amount = new Amount(Int32.MaxValue);
            Assert.Throws<OverflowException>(() => 
                    Program.performTransfer(amount, balance));
        }
        [Test]
        public void should_prevent_toctou_overdraw()
        {
            Balance balance = new Balance(1000);
            Amount amount = new Amount(1000);
            Amount amountNotAllowed = new Amount(1000);
            Assert.IsTrue(Program.performTransfer(amount, balance));
            Program.performTransfer(amountNotAllowed, balance);
            Assert.That(balance.value == 0, Is.True.After(2500));
        }


    }
}
